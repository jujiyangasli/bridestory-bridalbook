
#bridalbook-data

Download data "wedding-supliers" dari [http://www.bridalbook.ph](http://www.bridalbook.ph)

##usage
```javascript
node bridalbook-data.js
```

###help
```javascript
node bridalbook-data.js -h
node bridalbook-data.js --help
```

###Download satu suplier
```javascript
node bridalbook-data.js -u [url]
```

###Download banyak suplier
```javascript
node bridalbook-data.js -f [file]

// url list, separated by newline (utf-8)
```

###clear cache
```javascript
node bridalbook-data.js -clean
```

-------

####install
```javascript
git clone git@bitbucket.org:jujiyangasli/bridestory-bridalbook.git
cd bridestory-bridalbook
npm install
npm test
```