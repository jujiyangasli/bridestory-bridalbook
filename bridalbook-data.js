


var cli = require('./modules/cli-app.js');

function saveCSV (data){

	require('./modules/save-csv.js')
	.save(data)
	.then(function(){

		console.log('DONE!');
		console.log('');
		process.exit(0);

	});

}

if( global.VENDOR ){
	var vendor = require('./modules/fetch-vendor.js')
	vendor.fetch(global.VENDOR).then( function(data){

		console.log('');
		for(var i in data)
			console.log(i+': '+data[i]);
		saveCSV( [data] );
		
	} );
}

else if( global.VENDORLIST ){
	var vendorList = require('./modules/fetch-vendor-list.js')
	vendorList.fetch(global.VENDORLIST).then( saveCSV );
}

else {
	var fullapp = require('./modules/fullapp.js')
	fullapp.fetch().then( saveCSV );
}
