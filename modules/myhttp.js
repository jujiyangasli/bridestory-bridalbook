// GET http request with caching & failsafe

var q = require('q');
var md5 = require('MD5');
var cheerio = require('cheerio');
var ua = require('random-ua');
var fs = require('q-io/fs');
var fsn = require('fs');
var nofail = require('./noFailRequest.js');
var myRequest = require('./myRequest.js');

var cachedir = './htcache';
fs.exists(cachedir).then(function(d){ 
	if(!d) fs.makeDirectory(cachedir) 
});

function myhttp(){
	this.url = '';
	this.ua = '';
	this.failsafe = true;
	this.failtimeout = 1000;
	this.cache = true;
	this.returnString = false;
}

myhttp.prototype.request = function(url){
	
	
	if(url) this.url = url;
	var md = md5(this.url);
	var d = q.defer();
	var t = this;

	//use cache
	if(this.cache && fsn.existsSync(cachedir+'/'+md)) {
	
		var bo = fsn.readFileSync(cachedir+'/'+md,'utf8');
		if(t.returnString) d.resolve(bo);
		else d.resolve(cheerio.load(bo));
		return d.promise;
	}

	//not using cache
	var transport;
	if(this.failsafe) transport = new nofail();
	else transport = myRequest;


	transport.request({
		url: this.url,
		headers : {'user-agent': this.ua}
	},this.failtimeout).then(function(body){

		fs.write(cachedir+'/'+md, body);
		if(t.returnString) d.resolve(body);
		else d.resolve(cheerio.load(body));

	}).fail(function(status){
		d.reject(status);
	})

	return d.promise;
}

module.exports = exports = myhttp;

