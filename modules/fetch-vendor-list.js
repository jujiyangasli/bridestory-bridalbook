
var Vendor = require('./fetch-vendor.js');
var fs = require('fs');
var q = require('q');

var urls = []
var idx = 0;
var vendorData = [];
var loader = q.defer();

function fetchThem(){

	if(idx>=urls.length) {
		loader.resolve( vendorData );
	}

	Vendor.fetch(urls[idx])
	.then(function(data){
		vendorData.push(data);
		idx++;
		fetchThem();
	});

	return loader.promise;
}

function fetchVendors(file){

	if(!fs.existsSync(file)){
		global.errorString('file '+file+' not exists');
	}

	urls = fs.readFileSync( file, 'utf8' ).split("\n");
	return fetchThem();
}

module.exports = exports = { fetch : fetchVendors };

