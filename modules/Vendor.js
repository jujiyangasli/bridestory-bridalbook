
var http = require('./myhttp.js');
var urlParse = require('url');
var cheerio = require('cheerio');
var q = require('q');
var htmlentity = new (require('html-entities').AllHtmlEntities)();

var Vendor = function(name,url){

	this.name = 0;
	this.category = 0;
	this.img = 0;
	this.addr = 0;
	this.email = 0;
	this.tel = 0;
	this.url = 0;
	this.web = 0;
	
	this.name = name;
	this.url = url;
	this.http = new http();
	this.http.returnString = true;

	this.loader = q.defer();
};

function isEmail(str){
	return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(str);
};

function isExternal(url, base){
	var b = urlParse.parse(base).hostname.replace(/www\./,'');//.replace(/./g,'\.');
	return !(new RegExp('^(http(s|)://)(www.|)'+b).test(url));
}
function replaceBreak(str){
	return str.replace(/\r\n/g,"\n").replace(/\r/g,"\n").replace(/\n/g," ").replace(/\s+/g," ");
}

function trim(str){
	return replaceBreak(str).replace(/^\s+/,'').replace(/\s+$/,'');
}

Vendor.getName = function(body){
	return replaceBreak(body).match(/property\="og\:title".*?content\="([^"]+)"/)[1].split(' | ')[0];
};

Vendor.getCategory = function($){
	return $($('#content-main-result .breadcrumbs').first().find('a').get(2)).text();
};

Vendor.getImage = function(body){
	var img = replaceBreak(body).match(/property\="og\:image".*?content\="([^"]+)"/)[1];
	if( img.match(/\/uploads\//) )
		return img;
	else {
		return 'http://www.bridalbook.ph/templates/blackbook_v2/css/images/default.png';
	};
};

Vendor.getEmail = function($){
	var a,r,j,s='';
	if(!$('[data-cfemail]').length) return '';
	a = $('[data-cfemail]').attr('data-cfemail');
	r = parseInt( a.substr( 0, 2 ), 16 );
	for(j=2;a.length-j;j+=2){
		c = parseInt( a.substr( j, 2 ), 16 ) ^ r;
		s += String.fromCharCode(c);
	}
	s = trim( s );
	return isEmail(s) ? s : '';
}

Vendor.getContact = function($){
	var html = $('#more_contact').html();
	var $$ = cheerio.load(html);
	
	var tel = [];
	var t = $$('a[href^="tel:"]').each(function(){
		tel.push( trim( $(this).text() ) );
	});
	
	return {
		address : cheerio.load( 
					'<div>'+trim(html.replace(/<br(\s|)\/>/g,'<br>').split('<br>')[0])+'</div>' 
				)('div').text(),

		tel : tel.join(', '),

		web : trim( $$('a[href^="http://"]').last().attr('href') )
	}
};

Vendor.prototype.fetch = function(){
	var t = this;
	
	this.http.request(this.url)
	.then(function(str){

		try{

			var $ = cheerio.load(str);
			//console.log('\t load ok');
			var c = Vendor.getContact($);
			//console.log('\t contact ok');
			t.name = trim( Vendor.getName(str) );
			//console.log('\t name ok');
			t.img = trim( Vendor.getImage(str) );
			//console.log('\t image ok');
			t.category = trim( Vendor.getCategory($) );
			//console.log('\t category ok');
			t.addr = trim( c.address );
			//console.log('\t address ok');
			t.email = Vendor.getEmail($);
			//console.log('\t email ok');
			t.tel = c.tel;
			//console.log('\t tel ok');
			t.web = isExternal(c.web,t.url) ? c.web : '';
			//console.log('\t web ok');
			t.loader.resolve(true);

		}catch(e){
			setTimeout(function(){
				t.fetch();
			},1000);
		}
	});
	return this.loader.promise;
};

Vendor.prototype.toObject = function(){
	return {
		name: htmlentity.decode(this.name), 
		address: htmlentity.decode(this.addr),
		image: htmlentity.decode(this.img), 
		category: htmlentity.decode(this.category),
		email: htmlentity.decode(this.email), 
		tel: htmlentity.decode(this.tel),
		web: htmlentity.decode(this.web), 
		url: htmlentity.decode(this.url)
	};
};

module.exports = exports = Vendor;