var csv = require('fast-csv');
var q = require('q');

function savecsv(data){

    console.log('');
    console.log('Writing CSV data to ' + global.CSVFILE);
    console.log('');

    var d = q.defer();
    csv.writeToPath( global.CSVFILE, data, {
     headers: true,
     transform: function(row){
         return {
             Nama: row.name,
             Category: row.category,
             "Profile Picture URL": row.image,
             Alamat: row.address,
             Email: row.email,
             Telephone: row.tel,
             Website: row.web,
             URL: row.url
         };
     }
    }).on("finish", function(){
        d.resolve('ok');
    });

    return d.promise;
}

module.exports = exports = { save : savecsv };