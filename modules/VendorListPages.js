var http = require('./myhttp.js');
var q = require('q');
var urlParse = require('url');
var ev = require('events');

// the vendor list scraper
var VendorListPages = function(url){
	this.totalVendor = 0;
	this.currentPage = 1;
	this.lastPage = 0;
	this.url = url;
	this.curl = '';
	this.listed = [];
	this.loadStatus = q.defer();

	this.http = new http();
	
	this.baseurl = urlParse.parse(this.url);
	this.baseurl.pathname += '/all';

}

VendorListPages.fetchCurrentPage = function(){
	this.baseurl.query = {page:this.currentPage}
	return this.http.request(urlParse.format(this.baseurl));
}

VendorListPages.getTotalVendor = function($){

	var t = 0;
	$('section .anchor_special').each(function(){
		var to = 0;
		to = $(this).text().match(/\d+\-\d+\sof\s(\d+)/ )[1]*1;
		if( to ) t = to;
	});
	return t;
}

VendorListPages.getLastPage = function($){
	return $('#pagination #page-last').attr('href').match(/page=(\d+)/)[1];
}

VendorListPages.getVendors = function($){
	var r = [];
	$('#content_column h4').each(function(){
		var a = $(this).find('a');
		r.push({
			name: a.attr('title'),
			url: a.attr('href')
		});
	});

	return r;
}

VendorListPages.prototype.init = function(instance){

	//
	var t = this;
	t.totalVendor = 0;
	t.lastPage = 0;
	t.listed = [];
	t.currentPage = 1;

	if(instance){
		instance.textStatus = 'Fetching List '+this.currentPage+' of '+this.lastPage;
	}
	
	return VendorListPages.fetchCurrentPage.call(this)
	.then(function($){	
		try{
			t.totalVendor = VendorListPages.getTotalVendor($);
			t.lastPage = VendorListPages.getLastPage($);
			t.listed = t.listed.concat(VendorListPages.getVendors($));
			t.currentPage++;
			return true;

		}catch(e){
			return t.init(instance);
		}
	});
}

VendorListPages.prototype.fetchAll = function( instance ){

	var t = this;
	
	if(instance)
	instance.textStatus = 'Fetching List '+this.currentPage+' of '+this.lastPage;

	if(this.currentPage<=this.lastPage){

		VendorListPages.fetchCurrentPage.call(this)
		.then(function($){
			try{
				t.listed = t.listed.concat( VendorListPages.getVendors($) );
				t.currentPage++;
				t.fetchAll(instance);
			}catch(e){
				setTimeout(function(){
					t.fetchAll(instance);
				},1000);
			}
		});

	}else this.loadStatus.resolve(true);

	return this.loadStatus.promise;
}

module.exports = exports = VendorListPages;