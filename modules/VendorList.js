// get vendor list under a category from bridalbook.ph

var VendorListPages = require('./VendorListPages.js');
var Vendor = require('./Vendor.js');

var VendorList = function(category,url){

	this.length = 0;
	this.status = 0;
	this.textStatus = '';
	this.list = [];
	this.url = url;
	this.category = category;

	var t = this;
	var page = new VendorListPages(url);
	var fetchVendors = function(){

		if(t.length>=t.list.length) {
			t.status = 1;
			t.textStatus = 'DONE';
			return true;
		}

		t.textStatus = 'Fetching Vendor > '+t.list[t.length].name+
						' ('+(t.length+1)+' of '+page.totalVendor+')';

		return t.list[t.length].fetch()
				.then(function(){
					t.length++;
					return fetchVendors();
				});
	}; 

	this.fetch = function(){

		this.textStatus = 'Fetching List..';
		var t = this;
	
		return page.init(this)
		.then(function(){
			return page.fetchAll(t);
		})
		.then(function(){

			for(var i in page.listed)
				t.list.push( new Vendor( page.listed[i].name, page.listed[i].url ) );
			return fetchVendors();

		});	
	}	
}

module.exports = exports = VendorList;