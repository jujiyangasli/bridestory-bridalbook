// request with retry on fail

var http = require('./myRequest.js');
var q = require('q');

var sendReq = function(obj,timoue){
	var t = this;
	http.request(obj)	
	.then(function(body){
		t.loader.resolve(body);
	}).fail(function(){
		setTimeout(function(){
			sendReq.call(t,obj,timeout);
		},timeout);
	});
}

function nofailrequest(){
	this.loader = q.defer();
}

nofailrequest.prototype.request = function(obj,timeout){
	this.loader = q.defer();
	sendReq.call(this,obj,timeout);
	return this.loader.promise;
}

module.exports = exports = nofailrequest;