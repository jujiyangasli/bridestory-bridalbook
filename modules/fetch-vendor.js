
var Vendor = require('./Vendor.js');
var q = require('q');

function fetchVendor(url){
	
	var d = q.defer();
	var v = new Vendor('name',url);

	if(!url.match(/^http(s|)\:\/\/(www\.|)bridalbook\.ph\/wedding-suppliers\//)){
		global.errorString('invalid url: '+url);
		process.exit(1);
	}


	v.fetch()
	.then(function(){
		d.resolve( v.toObject() );
	});

	return d.promise;
}

module.exports = exports = { fetch : fetchVendor };