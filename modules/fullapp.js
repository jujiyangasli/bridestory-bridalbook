
var cat = require('./category.js');
var VendorList = require('./VendorList.js');
var catURL = 'http://www.bridalbook.ph/wedding-suppliers';
var q = require('q');

var categories = {};

var interval = false;
var loader = q.defer();

function fetchCategory(){

	console.log('Fetching category...');
	cat.getCategory(catURL)
	.fail(function(){
		console.log('ERROR: failed to get category..');
		console.log(arguments);
	})
	.then(function(c){
		console.log('Category Fetched..');
		fetchVendorList(c);
	});	

	return loader.promise;
}

function fetchVendorList(c){
	categories = c;
	console.log('');
	for(var i in categories){
		console.log(i);
		categories[i] = new VendorList(i,categories[i]);
		categories[i].fetch().done(function(){
			for(var j in categories) if(!categories[j].status) return;
			saveCSV();
		});
	}

	interval = setInterval(function(){
		displayStatus();
	},500);
}

function saveCSV(){
	var list = [];
	for(var j in categories){
		list.push(categories[j].toObject());
	}

	loader.resolve(list);
}

function displayStatus(){
	for(var i in categories){
		process.stdout.clearLine();
		process.stdout.moveCursor(0,-1);
	}

	for(var j in categories)
		console.log(categories[j].category+': '+categories[j].textStatus);
}


//start app
module.exports = exports = { fetch : fetchCategory };
