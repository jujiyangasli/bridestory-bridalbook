// GET http request with caching

var http = require('q-io/http');
var q = require('q');
var encoding = require('encoding');

function fetch(obj){

	var d = q.defer();
	http.request(obj)
	.then(function(response){
		var body = '';
		var charset = response.headers['content-type'].match(/charset\=(.*?)(;|$)/)[1];
		if(response.status!=200) d.reject(response.status);
		else
		response.body.forEach(function(s){
			body += s;
		}).then(function(s){
			if(s) return;
			d.resolve(
				encoding.convert(body,'UTF-8',charset).toString()
			);
		});

	}).fail(function(){
		d.reject('http failed miserably..');
	});

	return d.promise;
}

module.exports = exports = { request : fetch };