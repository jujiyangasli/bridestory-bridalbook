
function printHelp(){
	console.log('');
	console.log('\tcli app to download vendor data from  www.bridalbook.ph');
	console.log('');
	console.log('\tUsage:');
	console.log('\tnode bridalbook-data.js [options]');
	console.log('');
	console.log('\tOptions');
	console.log('\t  -h, --help\t print this help and exit');
	console.log('\t  --clean\t clean cache and exit');
	console.log('\t  -u [url]\t Download Vendor data from given [url]');
	console.log('\t  -f [file]\t Download Vendor data from given [utf8-file] (url list, separated by newline)');
	console.log('\t  -o [file]\t Output file, default to data.csv');
	console.log('');
}

function cleanCache(){
	if(require('fs').existsSync('htcache')){
		require('fs.extra').rmrfSync('htcache');
	}
	console.log('cache cleaned');
}

if(process.argv.indexOf('-h')>-1 || process.argv.indexOf('--help')>-1){
	printHelp();
	process.exit(0);
}

if(process.argv.indexOf('--clean')>-1){
	cleanCache();
	process.exit(0);	
}

if(process.argv.indexOf('-o')>-1){
	global.CSVFILE = process.argv[ process.argv.indexOf('-o')+1 ];
}else{
	global.CSVFILE = 'data.csv';
}

if(process.argv.indexOf('-u')>-1){
	global.VENDOR = process.argv[ process.argv.indexOf('-u')+1 ];
}else{
	global.VENDOR = false;
}

if(process.argv.indexOf('-f')>-1){
	global.VENDORLIST = process.argv[ process.argv.indexOf('-f')+1 ];
}else{
	global.VENDORLIST = false;
}

global.errorString = function(str){
	console.log('');
	console.log('\tERROR '+str);
	console.log('\t==============================================');
	printHelp();
	process.exit(1);
}

if(global.VENDOR && global.VENDORLIST){
	global.errorString('cannot use -u and -f at the same time');
}