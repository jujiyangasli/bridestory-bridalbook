// get category from bridalbook.ph

var http = require('./myhttp.js');

module.exports = exports = {
	getCategory : function( url ){
		var ht = new http();
		//ht.cache = false;
		return ht.request(url)
		.then(function($){
			var cat = {};
			$('#content_column h3').each(function(){
				var a = $(this).find('a');
				cat[a.text()] = a.attr('href');
			});
			return cat;
		});
	}
}

