var category = require("../modules/category.js");
var VendorList = require('../modules/VendorList.js');
var VendorListPages = require("../modules/VendorListPages.js");
var Vendor = require("../modules/Vendor.js");
var assert = require('assert');

describe('Category', function(){
	describe('#fetch',function(){
		this.timeout(5000);
		it('should fetch categories and url from the website', function(done){
		  	var catURL = 'http://www.bridalbook.ph/wedding-suppliers';
		  	category.getCategory(catURL)
		  	.then(function(c){
		  		for(var i in c){
		  			assert.ok(
		  				c[i].match(/^http\:\/\/www\.bridalbook\.ph\/wedding-suppliers\//)
		  			);
		  		}
		  		done();
		    });
	  	});
	});
});

//var vlist = new VendorList();

var v = new VendorListPages('http://www.bridalbook.ph/wedding-suppliers/wedding-invitations');

describe('VendorList', function(){
	describe('#init',function(){
		this.timeout(5000);
		it('should get the page-length and total-vendor in the "wedding-invitations" category',function(done){
			v.init()
			.then(function(){
				var err = [];
				assert.ok(v.totalVendor);
				assert.ok(v.lastPage);
				assert.ok(v.listed.length);
				assert.equal(v.currentPage,2);
				done();

			});
	
		});
	});

	describe('#fetchall',function(){
		this.timeout(30000);
		it('should get all the page-data in the "wedding-invitations" category',function(done){
			v.fetchAll()
			.then(function(){

				assert.equal(v.listed.length, v.totalVendor, 'listed length is wrong');
				assert.equal(--v.currentPage,v.lastPage,'currentPage is wrong');
				done();

			});
	
		});
	});

});

describe('Vendor', function(){
	describe('#fetch', function(){

		this.timeout(5000);
		it('should get the data from a random vendor of the category "wedding-invitations"',function(done){

			var idx = Math.round(Math.random()*v.listed.length);
			var name = v.listed[idx].name;
			var url = v.listed[idx].url;
			var vendor = new Vendor(name, url);
			console.log('\t'+url);
			vendor.fetch()
			.then(function(){

				var f = vendor.toObject();
				for(var i in f){
					console.log('\t '+i+': '+f[i]);
				}

				done();

			});

		});

	});
});